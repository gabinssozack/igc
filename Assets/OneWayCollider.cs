﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneWayCollider : MonoBehaviour
{
    public PlatformEffector2D pE;
    public float waitingTime;
    private float waitingTimeCounter;
    public bool isJumpDown = false;

    // Start is called before the first frame update
    void Start()
    {
        pE = GetComponent<PlatformEffector2D>();
        waitingTimeCounter = waitingTime;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetAxis("Vertical") < 0)
        {
            if(waitingTimeCounter <= 0)
            {
                pE.rotationalOffset = 180f;
                waitingTimeCounter = waitingTime;
                isJumpDown = true;
            }
            else
            {
                waitingTimeCounter -= Time.deltaTime;
            }
        }
        else if(Input.GetAxis("Vertical") >= 0)
        {
            pE.rotationalOffset = 0f;
        }
    }
}
