﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataEnemyCreate
{
    public int damage;
    public int hp;
    public int rof;
    public int fallBack;
}

public class EnemyControl : FSMSystem
{
    public int damage;
    public bool isAlive;
    public float timeDelay;
    public int fallBack;
    private int hp;
    private System.Action dieCallBack;
    public int Hp
    {
        get
        {
            return hp;
        }
        set
        {
            hp -= value;


        }
    }
    public int rof;

    public virtual void Setup(DataEnemyCreate data, System.Action dieCallBack = null)
    {
        this.damage = data.damage;
        this.hp = data.hp;
        this.rof = data.rof;
        this.fallBack = data.fallBack;
        this.dieCallBack = dieCallBack;
    }

    public virtual void OnAttack()
    {
        Debug.Log("Enemy Attack");
    }

    public virtual void OnDamage(int dmg)
    {
        this.hp -= dmg;
        if (hp <= 0)
        {
            dieCallBack?.Invoke();
            Destroy(gameObject);
        }
    }
    public virtual void OnDamage(int dmg, System.Action<object> callback)
    {
        this.hp -= dmg;
        if (hp <= 0)
        {
            dieCallBack?.Invoke();
            Destroy(gameObject);
        }
       
    }
}