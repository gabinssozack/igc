﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Enemy_02_DeadState : FSMState
{
    [System.NonSerialized]
    public Enemy_02_Control parent;
}
