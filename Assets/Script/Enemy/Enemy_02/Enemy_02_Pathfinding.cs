﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using System;

public class Enemy_02_Pathfinding : MonoBehaviour
{

    [HideInInspector]
    public Enemy_02_Control parent;
    public Transform target;

    public float speed;
    public float nextWaypointDistance;

    Path path;
    public int currentWaypoint = 0;
    public bool reachedTheEndPoint = false;


    Seeker seeker;
    Rigidbody2D rig;

    private void Start()
    {
        seeker = GetComponent<Seeker>();
        rig = GetComponent<Rigidbody2D>();
        target = GameObject.FindGameObjectWithTag("Player").transform;
        InvokeRepeating("PathFinding", 0f, 0.5f);

    }

    private void FixedUpdate()
    {
            
        FollowThePath();
       
    }
    public void FollowThePath()
    {
        if(parent.isFindind)
        {

            if (path == null)
            {
                return;
            }
            if (currentWaypoint >= path.vectorPath.Count)
            {
                reachedTheEndPoint = true;
                return;
            }
            else
            {
                reachedTheEndPoint = false;
            }

            Vector2 dir = ((Vector2)path.vectorPath[currentWaypoint] - rig.position).normalized;
            rig.AddForce (dir * speed*Time.deltaTime);

            float distance = Vector2.Distance(transform.position, path.vectorPath[currentWaypoint]);
            if (distance < nextWaypointDistance)
            {
                currentWaypoint++;
            }
        }
    }

    public void PathFinding()
    {
        if(seeker.IsDone())
            seeker.StartPath(transform.position, target.position, OnPathComplete);
    }

    private void OnPathComplete(Path p)
    {
        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
    }
}
