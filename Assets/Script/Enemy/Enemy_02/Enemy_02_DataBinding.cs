﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_02_DataBinding : MonoBehaviour
{
    public Animator anim;
    public float MoveSpeed
    {
        set
        {
            anim.SetFloat(key_MoveSpeed, value);
        }
    }

    private int key_MoveSpeed;

    void Start()
    {
        key_MoveSpeed = Animator.StringToHash("MoveSpeed");

    }
}
