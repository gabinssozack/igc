﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Enemy_02_StartState : FSMState
{
    [System.NonSerialized]
    public Enemy_02_Control parent;
    private float timeDelay;

    public override void OnEnter()
    {
            
        timeDelay = parent.timeDelay;
        base.OnEnter();
    }
    public override void FixedUpdate()
    {
        timeDelay -= Time.deltaTime;

        if(timeDelay <= 0)
        {
            parent.isFindind = true;
            parent.GoToState(parent.runState);
            
        }
    }
}