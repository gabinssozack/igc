﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Enemy_02_RunState : FSMState
{
    [System.NonSerialized]
    public Enemy_02_Control parent;

    public override void OnEnter()
    {
        Debug.Log("Enemy Run");
        parent.dataBinding.MoveSpeed = 0.1f;
        base.OnEnter();
    }
}

