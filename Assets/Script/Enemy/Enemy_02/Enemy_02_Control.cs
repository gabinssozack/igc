﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_02_Control : EnemyControl
{
    public bool isFindind = false;
    public Enemy_02_DeadState deadState;
    public Enemy_02_DataBinding dataBinding;
    public Enemy_02_StartState startState;
    public Enemy_02_Pathfinding pathfinding;
    public Enemy_02_RunState runState;
    public Transform target;
    public Transform trans;
    public float radius;
    public float timeHitDelay;
    private float timeHitDelayCount;
    public LayerMask mask;
    private DataEnemyCreate data = new DataEnemyCreate { damage = 10, rof = 10, hp = 10, fallBack = 200 };
    private void Start()
    {
        //Setup();
    }

    public override void Setup(DataEnemyCreate data, System.Action dieCallBack = null)
    {
        base.Setup(this.data, dieCallBack);
        trans = transform;

        startState.parent = this;
        AddState(startState);

        pathfinding.parent = this;

        deadState.parent = this;
        AddState(deadState);

        runState.parent = this;
        AddState(runState);
    }

    public override void SystemFixedUpdate()
    {
        if(timeHitDelay > 0)
        {
            timeHitDelay -= Time.deltaTime;
        }
        else
        {
            Collider2D hitInfor = Physics2D.OverlapCircle(trans.position, radius, mask);

            if(hitInfor != null)
            {
                hitInfor.GetComponent<PlayerControl>().OnDamaged(this.damage);
                timeHitDelayCount = timeHitDelay;
                GoToState(startState);
                isFindind = false;
            }

        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(trans.position, radius);
    }
}
