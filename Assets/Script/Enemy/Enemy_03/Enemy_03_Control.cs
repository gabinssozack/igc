﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_03_Control : EnemyControl
{
    public Enemy_03_AttackState attackState;
    public Enemy_03_DataBinding dataBinding;
    public Enemy_03_IdleState idleState;
    public Enemy_03_StartState startState;
    public Transform trans;
    public Transform bullet;
    public Transform target;
    public Transform muzzle;
    public Transform textture;
    public Rigidbody2D rig;
    public Vector2 dir;
    public float distance;
    public LayerMask layerMask;
    public float timeCount;
    private DataEnemyCreate data = new DataEnemyCreate() { hp = 5, damage = 1, rof = 3, fallBack = 0 };
    private void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        //Setup();
    }
    public override void Setup(DataEnemyCreate data, System.Action dieCallBack = null)
    {
        base.Setup(this.data, dieCallBack);

        trans = transform;
        target = GameObject.FindGameObjectWithTag("Player").transform;

        startState.parent = this;
        AddState(startState);

        attackState.parent = this;
        AddState(attackState);

        idleState.parent = this;
        AddState(idleState);
    }

    public override void SystemFixedUpdate()
    {
        checkGround();
        dir = target.position - muzzle.position;
        dir.Normalize();

        float dot = Vector3.Dot(dir, Vector3.right);
        if (dot > 0)
        {
            trans.localScale = new Vector3(1f, 1f, 1f);
        }
        if (dot < 0)
        {
            trans.localScale = new Vector3(-1f, 1f, 1f);
        }
    }

    public Transform createBullet()
    {
        Vector3 dir2 = target.position - muzzle.position;
        dir2.Normalize();
        Transform _bullet =  Instantiate(bullet);
        _bullet.SetParent(null);
        _bullet.localPosition = muzzle.position;
        _bullet.right = dir2;
        _bullet.GetComponent<Bullet>().Setup(this.damage);
        return _bullet;
    }
    public void checkGround()
    {
        RaycastHit2D hitInfor = Physics2D.Raycast(textture.position, Vector2.down, distance, layerMask);
        if(hitInfor.collider!= null)
        {
            rig.velocity = new Vector2(rig.velocity.x,0f);
        }
        else
        {
            rig.velocity = new Vector2(rig.velocity.x, -9.8f);
        }

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        Gizmos.DrawRay(textture.position, Vector2.down * distance);
    }
}
