﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Enemy_03_AttackState : FSMState
{
    [NonSerialized]
    public Enemy_03_Control parent;

    public override void OnEnter()
    {
        parent.dataBinding.Attack = true;

    

        parent.GoToState(parent.idleState);
    }
}
