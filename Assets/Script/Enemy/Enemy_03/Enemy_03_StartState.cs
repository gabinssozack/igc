﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Enemy_03_StartState : FSMState
{
    [NonSerialized]
    public Enemy_03_Control parent;
    private float timeDelay;
    public override void OnEnter()
    {
        this.timeDelay = parent.timeDelay;
    }

    public override void FixedUpdate()
    {
        timeDelay -= Time.deltaTime;

        if(timeDelay <= 0)
        {
            parent.GoToState(parent.attackState);
        }
    }
}
