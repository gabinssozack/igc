﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_03_DataBinding : MonoBehaviour
{
    public Animator anim;
    public bool Attack
    {
        set
        {
            if (value)
            {
                anim.SetBool(key_Attack, value);
            }
        }
    }

    private int key_Attack;
    void Start()
    {
        key_Attack = Animator.StringToHash("Attack");
    }
}
