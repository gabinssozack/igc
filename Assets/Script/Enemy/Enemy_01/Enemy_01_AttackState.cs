﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Enemy_01_AttackState : FSMState
{
    [NonSerialized]
    public Enemy_01_Control parent;
    public float delay;

    public override void OnEnter()
    {
        delay = parent.attackTimeDelay;
        base.OnEnter();
    }
    public override void FixedUpdate()
    {
        delay -= Time.deltaTime;
        if(delay <= 0)
        {
            parent.GoToState(parent.moveState);
        }
    }
}
