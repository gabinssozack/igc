﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_01_Control : EnemyControl
{
    public Enemy_01_AttackState attackState;
    public Enemy_01_DeadState deadState;
    public Enemy_01_MoveState moveState;
    public Enemy_01_StartState startState;
    public Enemy_01_DataBinding dataBinding;
    public Transform headTrans;
    public Transform trans;
    public Transform texture;
    public float distance;
    public float radius;

    public float attackTimeDelay;

    public bool isGround;
    public float moveSpeed;
    public int startDir;
    public LayerMask groundMask;
    public LayerMask playerMask;
    private DataEnemyCreate data = new DataEnemyCreate() { hp = 3, damage = 1, rof = 0, fallBack = 0 };
    private void Start()
    {
        //Setup();
    }

    public override void Setup(DataEnemyCreate data, System.Action dieCallBack = null)
    {
        base.Setup(this.data, dieCallBack);

        trans = transform;


        startState.parent = this;
        AddState(startState);

        moveState.parent = this;
        AddState(moveState);

        deadState.parent = this;
        AddState(deadState);

        attackState.parent = this;
        AddState(attackState);

        startDir = Random.Range(0, 100);
        if(startDir >=  50)
        {
            DoRotate();
        }
        
    }

    public override void SystemFixedUpdate()
    {
        RaycastHit2D hitGroundInfor = Physics2D.Raycast(headTrans.position, trans.right, distance, groundMask);
        Collider2D checkGroundInfor = Physics2D.OverlapCircle(trans.position, radius, groundMask);
        isGround = checkGroundInfor;
        Collider2D hitPlayerInfor = Physics2D.OverlapCircle(trans.position, radius, playerMask);
        if (hitGroundInfor.collider || hitPlayerInfor != null)
        {
            dataBinding.MoveSpeed = 0;
            if (hitPlayerInfor != null)
            {
                hitPlayerInfor.GetComponent<PlayerControl>().OnDamaged(this.damage);
            }
            GoToState(attackState);
        }
        
    }
    public void DoRotate()
    {
        trans.right *= -1;
        texture.localScale = new Vector3(-1 * texture.localScale.x, texture.localScale.y, texture.localScale.z);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(trans.position, radius);
        Gizmos.DrawRay(headTrans.position, trans.right*distance);
    }
}
