﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Enemy_01_MoveState : FSMState
{
    [NonSerialized]
    public Enemy_01_Control parent;

    public override void OnEnter()
    {
        base.OnEnter();
        parent.dataBinding.MoveSpeed = 0.2f;
    }

    public override void FixedUpdate()
    {
        if(parent.isGround)
        {
            parent.trans.Translate(Vector2.right * parent.moveSpeed * Time.deltaTime);
        }
        else
        {
            parent.trans.Translate(-Vector2.up * 9.8f * Time.deltaTime);
        }
    }
    public override void OnExit()
    {
        parent.DoRotate();
    }
}
