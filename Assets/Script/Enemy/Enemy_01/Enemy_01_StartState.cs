﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Enemy_01_StartState : FSMState
{
    [NonSerialized]
    public Enemy_01_Control parent;
    private float delayTime;

    public override void OnEnter()
    {
        this.delayTime = parent.timeDelay;
        base.OnEnter();
    }

    public override void FixedUpdate()
    {
        delayTime -= Time.deltaTime;
        if(delayTime <= 0)
        {
            parent.GoToState(parent.moveState);
        }
    }
}
