﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Enemy_01_IdleState : FSMState
{
    [NonSerialized]
    public Enemy_01_Control parent;
}
