﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class test : MonoBehaviour
//{
//    [SerializeField]
//    LayerMask lmWalls;
//    [SerializeField]
//    float fJumpVelocity = 5;

//    Rigidbody2D rigid;

//    float fJumpPressedRemember = 0;
//    [SerializeField]
//    float fJumpPressedRememberTime = 0.2f;

//    float fGroundedRemember = 0;
//    [SerializeField]
//    float fGroundedRememberTime = 0.25f;

//    [SerializeField]
//    float fHorizontalAcceleration = 1;
//    [SerializeField]
//    [Range(0, 1)]
//    float fHorizontalDampingBasic = 0.5f;
//    [SerializeField]
//    [Range(0, 1)]
//    float fHorizontalDampingWhenStopping = 0.5f;
//    [SerializeField]
//    [Range(0, 1)]
//    float fHorizontalDampingWhenTurning = 0.5f;

//    [SerializeField]
//    [Range(0, 1)]
//    float fCutJumpHeight = 0.5f;

//    void Start()
//    {
//        rigid = GetComponent<Rigidbody2D>();
//    }

//    void Update()
//    {
//        Vector2 v2GroundedBoxCheckPosition = (Vector2)transform.position + new Vector2(0, -0.01f);
//        Vector2 v2GroundedBoxCheckScale = (Vector2)transform.localScale + new Vector2(-0.02f, 0);
//        bool bGrounded = Physics2D.OverlapBox(v2GroundedBoxCheckPosition, v2GroundedBoxCheckScale, 0, lmWalls);

//        fGroundedRemember -= Time.deltaTime;
//        if (bGrounded)
//        {
//            fGroundedRemember = fGroundedRememberTime;
//        }

//        fJumpPressedRemember -= Time.deltaTime;
//        if (Input.GetButtonDown("Jump"))
//        {
//            fJumpPressedRemember = fJumpPressedRememberTime;
//        }

//        if (Input.GetButtonUp("Jump"))
//        {
//            if (rigid.velocity.y > 0)
//            {
//                rigid.velocity = new Vector2(rigid.velocity.x, rigid.velocity.y * fCutJumpHeight);
//            }
//        }

//        if ((fJumpPressedRemember > 0) && (fGroundedRemember > 0))
//        {
//            fJumpPressedRemember = 0;
//            fGroundedRemember = 0;
//            rigid.velocity = new Vector2(rigid.velocity.x, fJumpVelocity);
//        }

//        float fHorizontalVelocity = rigid.velocity.x;
//        fHorizontalVelocity += Input.GetAxisRaw("Horizontal");

//        if (Mathf.Abs(Input.GetAxisRaw("Horizontal")) < 0.01f)
//            fHorizontalVelocity *= Mathf.Pow(1f - fHorizontalDampingWhenStopping, Time.deltaTime * 10f);
//        else if (Mathf.Sign(Input.GetAxisRaw("Horizontal")) != Mathf.Sign(fHorizontalVelocity))
//            fHorizontalVelocity *= Mathf.Pow(1f - fHorizontalDampingWhenTurning, Time.deltaTime * 10f);
//        else
//            fHorizontalVelocity *= Mathf.Pow(1f - fHorizontalDampingBasic, Time.deltaTime * 10f);

//        rigid.velocity = new Vector2(fHorizontalVelocity, rigid.velocity.y);
//    }
//}










//public class MovementControler : MonoBehaviour
//{

//    private float movementInputDirection;

//    private int amountOfJumpsLeft;
//    private int facingDirection = 1;

//    private bool isFacingRight = true;
//    private bool isWalking;
//    private bool isGrounded;
//    private bool isTouchingWall;
//    private bool isWallSliding;
//    private bool canJump;

//    private Rigidbody2D rb;

//    private int amountOfJumps=2;

//    [SerializeField]
//    private float movementSpeed;
//    [SerializeField]
//    public float jumpForce;
//    [SerializeField]
//    public float groundCheckRadius;
//    [SerializeField]
//    public float wallCheckDistance;
//    [SerializeField]
//    public float wallSlideSpeed;
//    [SerializeField]
//    public float movementForceInAir;
//    [SerializeField]
//    public float airDragMultiplier;
//    [SerializeField]
//    public float variableJumpHeightMultiplier;
//    [SerializeField]
//    public float wallHopForce;
//    [SerializeField]
//    public float wallJumpForce;

//    [SerializeField]
//    public Vector2 wallHopDirection;
//    [SerializeField]
//    public Vector2 wallJumpDirection;

//    [SerializeField]
//    public Transform groundCheck;
//    [SerializeField]
//    public Transform wallCheck;

//    public LayerMask whatIsGround;

//    // Start is called before the first frame update
//    void Start()
//    {
//        rb = GetComponent<Rigidbody2D>();
//        amountOfJumpsLeft = amountOfJumps;
//        wallHopDirection.Normalize();
//        wallJumpDirection.Normalize();
//    }

//    // Update is called once per frame
//    void Update()
//    {
//        CheckInput();
//        CheckMovementDirection();
//        CheckIfCanJump();
//        CheckIfWallSliding();
//    }

//    private void FixedUpdate()
//    {
//        ApplyMovement();
//        CheckSurroundings();
//    }

//    private void CheckIfWallSliding()
//    {
//        if (isTouchingWall && !isGrounded && rb.velocity.y < 0)
//        {
//            isWallSliding = true;
//        }
//        else
//        {
//            isWallSliding = false;
//        }
//    }

//    private void CheckSurroundings()
//    {
//        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);

//        isTouchingWall = Physics2D.Raycast(wallCheck.position, transform.right, wallCheckDistance, whatIsGround);
//    }

//    private void CheckIfCanJump()
//    {
//        if ((isGrounded && rb.velocity.y <= 0) || isWallSliding)
//        {
//            amountOfJumpsLeft = amountOfJumps;
//        }

//        if (amountOfJumpsLeft <= 0)
//        {
//            canJump = false;
//        }
//        else
//        {
//            canJump = true;
//        }

//    }

//    private void CheckMovementDirection()
//    {
//        if (isFacingRight && movementInputDirection < 0)
//        {
//            Flip();
//        }
//        else if (!isFacingRight && movementInputDirection > 0)
//        {
//            Flip();
//        }

//        if (rb.velocity.x != 0)
//        {
//            isWalking = true;
//        }
//        else
//        {
//            isWalking = false;
//        }
//    }



//    private void CheckInput()
//    {
//        movementInputDirection = Input.GetAxisRaw("Horizontal");

//        if (Input.GetButtonDown("Jump"))
//        {
//            Jump();
//        }

//        if (Input.GetButtonUp("Jump"))
//        {
//            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * variableJumpHeightMultiplier);
//        }

//    }

//    private void Jump()
//    {
//        if (canJump && !isWallSliding)
//        {
//            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
//            amountOfJumpsLeft--;
//        }
//        else if (isWallSliding && movementInputDirection == 0 && canJump) //Wall hop
//        {
//            isWallSliding = false;
//            amountOfJumpsLeft--;
//            Vector2 forceToAdd = new Vector2(wallHopForce * wallHopDirection.x * -facingDirection, wallHopForce * wallHopDirection.y);
//            rb.AddForce(forceToAdd, ForceMode2D.Impulse);
//        }
//        else if ((isWallSliding || isTouchingWall) && movementInputDirection != 0 && canJump)
//        {
//            isWallSliding = false;
//            amountOfJumpsLeft--;
//            Vector2 forceToAdd = new Vector2(wallJumpForce * wallJumpDirection.x * movementInputDirection, wallJumpForce * wallJumpDirection.y);
//            rb.AddForce(forceToAdd, ForceMode2D.Impulse);
//        }
//    }

//    private void ApplyMovement()
//    {

//        if (isGrounded)
//        {
//            rb.velocity = new Vector2(movementSpeed * movementInputDirection, rb.velocity.y);
//        }
//        else if (!isGrounded && !isWallSliding && movementInputDirection != 0)
//        {
//            Vector2 forceToAdd = new Vector2(movementForceInAir * movementInputDirection, 0);
//            rb.AddForce(forceToAdd);

//            if (Mathf.Abs(rb.velocity.x) > movementSpeed)
//            {
//                rb.velocity = new Vector2(movementSpeed * movementInputDirection, rb.velocity.y);
//            }
//        }
//        else if (!isGrounded && !isWallSliding && movementInputDirection == 0)
//        {
//            rb.velocity = new Vector2(rb.velocity.x * airDragMultiplier, rb.velocity.y);
//        }

//        if (isWallSliding)
//        {
//            if (rb.velocity.y < -wallSlideSpeed)
//            {
//                rb.velocity = new Vector2(rb.velocity.x, -wallSlideSpeed);
//            }
//        }
//    }

//    private void Flip()
//    {
//        if (!isWallSliding)
//        {
//            facingDirection *= -1;
//            isFacingRight = !isFacingRight;
//            transform.Rotate(0.0f, 180.0f, 0.0f);
//        }
//    }

//    private void OnDrawGizmos()
//    {
//        Gizmos.DrawWireSphere(groundCheck.position, groundCheckRadius);

//        Gizmos.DrawLine(wallCheck.position, new Vector3(wallCheck.position.x + wallCheckDistance, wallCheck.position.y, wallCheck.position.z));
//    }
//}