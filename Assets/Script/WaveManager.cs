﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WaveManager : MonoBehaviour
{
    private int score;
    public int Score
    {
        get
        {
            return score;
        }
        set
        {
            score = value;
        }
    }
    public static WaveManager instance;
    public GAME_STATUS gameStatus = GAME_STATUS.PAUSE;

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    private void Start()
    {
        PlayerControl.Instance.CollectGun("Gun");
    }
    public void EndGame(int score)
    {
        Debug.Log(score);
        if(PlayerPrefs.GetInt("HighScore",0) <= score)
        {
            PlayerPrefs.SetInt("HighScore", score);
        }
        SceneManager.LoadScene(0);
        PoolManager.dictionaryPool.Clear();
        gameStatus = GAME_STATUS.LOST;
    }
}
public enum GAME_STATUS
{
    PAUSE = 1,
    PLAYING = 2,
    LOST = 3,
}