﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementControler : MonoBehaviour
{
    public float groundCheckRadius;


    private float horVelocity;
    private float verVelocity;
    private bool isFacingRight = true;
    private Rigidbody2D rig;
    private float groundedRemember = 0;
    private float jumpPressedRemember = 0;
    [SerializeField]
    private bool canJump;
    [SerializeField]
    private bool isGround;

    [SerializeField]
    [Range(0, 1)]
    private float horVelocityDampingWhenStopping;
    [SerializeField]
    [Range(0, 1)]
    private float horVelocityDampingBasic;
    [SerializeField]
    [Range(0, 1)]
    private float horVelocityDampingWhenTurning;
    [SerializeField]
    [Range(0, 1)]
    float cutJumpHeight = 0.5f;

    [SerializeField]
    float jumpPressedRememberTime = 0.2f;
    [SerializeField]
    float groundedRememberTime = 0.25f;
    [SerializeField]
    private float speed;
    [SerializeField]
    private float jumpForce;
    public int amountOfJump;
    [SerializeField]
    private Transform archorFoot_1;
    [SerializeField]
    private Transform archorFoot_2;
    [SerializeField]
    private LayerMask groundMask;
    [SerializeField]
    private LayerMask oneWayGroundMask;
    [SerializeField]
    private Transform texture;
    public Transform oneWay;

    private void Start()
    {
        rig = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        CheckInput();
        checkPosible();
        VerticalMovement();
        HorizontalMovement();
        checkOneWayGround();
        if (isGround && rig.velocity.y <= 0)
        {
            amountOfJump = 2;
            oneWay.GetComponent<OneWayCollider>().isJumpDown = false;
        }
    }


    private void CheckInput()
    {
        horVelocity = Input.GetAxis("Horizontal");
        groundedRemember -= Time.deltaTime;
        if (isGround)
        {
            groundedRemember = groundedRememberTime;
        }
        jumpPressedRemember -= Time.deltaTime;

        if (Input.GetButtonDown("Jump"))
        {
            jumpPressedRemember = jumpPressedRememberTime;
        }

    }

    private void HorizontalMovement()
    {
        if (horVelocity < 0 && isFacingRight)
        {
            Flip();
        }
        if (horVelocity > 0 && !isFacingRight)
        {
            Flip();
        }

        rig.velocity = new Vector2(horVelocity * speed, rig.velocity.y);

        horVelocity += Input.GetAxis("Horizontal");

        if (Mathf.Abs(Input.GetAxisRaw("Horizontal")) < 0.01f)
            horVelocity *= Mathf.Pow(1f - horVelocityDampingWhenStopping, Time.deltaTime * 10f);
        else if (Mathf.Sign(Input.GetAxisRaw("Horizontal")) != Mathf.Sign(horVelocity))
            horVelocity *= Mathf.Pow(1f - horVelocityDampingWhenTurning, Time.deltaTime * 10f);
        else
            horVelocity *= Mathf.Pow(1f - horVelocityDampingBasic, Time.deltaTime * 10f);

    }
    private void VerticalMovement()
    {
        if (Input.GetButtonUp("Jump"))
        {
            if (rig.velocity.y > 0)
            {
                rig.velocity = new Vector2(rig.velocity.x, rig.velocity.y * cutJumpHeight);
            }
            amountOfJump--;

        }
        if(amountOfJump>0)
        {
            if ((jumpPressedRemember > 0) && (groundedRemember > 0))
            {
                jumpPressedRemember = 0;
                groundedRemember = 0;
                rig.velocity = new Vector2(rig.velocity.x, jumpForce);
            }
            else if(Input.GetButtonDown("Jump"))
            {
                rig.velocity = new Vector2(rig.velocity.x, jumpForce);
            }
        }
    }

    private void Flip()
    {
        isFacingRight = !isFacingRight;
        texture.localScale = new Vector3(-1 * texture.localScale.x, texture.localScale.y, texture.localScale.z);
    }

    private void checkPosible()
    {
        isGround = Physics2D.OverlapCircle(archorFoot_1.position,groundCheckRadius,groundMask);
       
    }
    private void checkOneWayGround()
    {
        RaycastHit2D hitInfor = Physics2D.Raycast(archorFoot_1.position, Vector2.up * -1f,0.5f, oneWayGroundMask);
        if(hitInfor.collider != null)
        {
            
            Debug.Log("We touched one way ground");
            if(rig.velocity.y >=0)
            {
                hitInfor.collider.GetComponent<OneWayCollider>().isJumpDown = false;
            }
            if (!hitInfor.collider.GetComponent<OneWayCollider>().isJumpDown && rig.velocity.y < 0)
            {
                amountOfJump = 2;
            }
        }
        
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(archorFoot_1.position, groundCheckRadius);
        Gizmos.DrawRay(archorFoot_1.position, Vector2.down*0.5f);
    }
}
