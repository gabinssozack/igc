﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    private static PlayerControl instance;
    public static PlayerControl Instance => instance;
    [SerializeField]
    private int hp = 3;
    public int Hp
    {
        get
        {
            return hp;

        }
        set
        {
            hp = value;
            if(hp <= 0)
            {
                WaveManager.instance.EndGame(WaveManager.instance.Score);
            }
        }
    }

    public int damage;
    public bool isAvaible = true;
    public Transform currentGun;
    public Transform gunPos;
    public LayerMask gunCollectable;

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    private void FixedUpdate()
    {
        CollectGun();
    }

    public Transform InitGun(GameObject gun)
    {
        if (currentGun != null)
            Destroy(currentGun.gameObject);
        Transform _gun = Instantiate(gun).transform;
        _gun.position = gunPos.position;
        _gun.right = gunPos.right;
        _gun.SetParent(gunPos);
        

        currentGun = _gun;
        return _gun;
    }
    
    public void CollectGun()
    {
        Collider2D hit = Physics2D.OverlapBox(transform.position, transform.localScale, 0, gunCollectable);

        if(hit!=null)
        {
            Transform pos = InitGun((Resources.Load("Guns/" + hit.GetComponent<GunCollectableScript>().name,typeof(GameObject)) as GameObject));
            pos.GetComponent<GunBehavior>().Setup();
            Destroy(hit.gameObject);
            StartCoroutine(SpawnerManager.Instance.CreateGun((obj) => { 
                //Su ly sung tao ra
            }));
            WaveManager.instance.Score++;
        }
    }
    public void CollectGun(string name)
    {
        Transform pos = InitGun((Resources.Load("Guns/" + name, typeof(GameObject)) as GameObject));
        pos.GetComponent<GunBehavior>().Setup();
    }
    public void OnDamaged(int damage)
    {
        StartCoroutine(onDamage(damage));
    }
    IEnumerator onDamage(int damage)
    {
        if (isAvaible)
        {

            Debug.Log("Player Damaged");
            Hp -= damage;
            UIManager.Instance.deactiveHeart(hp);
            isAvaible = false;
            yield return new WaitForSeconds(1f);
            isAvaible = true;
        }
       
    }
    public void OnDamaged(object data)
    {

    }
}
