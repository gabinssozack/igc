﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour
{
    private static GunController instance;
    public static GunController Instance => instance;
    public Transform trans;

    public float rotationOffset;
    public bool isTheMouseInTheRightSide = true;
    public float mousePosition = 1;
    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    private void Start()
    {
        trans = transform;
    }
    // Update is called once per frame
    void Update()
    {
        mousePoint();
    }
    void mousePoint()
    {
        Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 posW = new Vector3(pos.x, pos.y, trans.position.z);
        Vector3 dir = posW - trans.position;

        posW.Normalize();

        trans.right = dir;

        float dot = Vector3.Dot(dir, Vector3.right);
        if(dot > 0)
        {
            trans.localScale = new Vector3(1, 0.3f, 1);
        }
        if (dot < 0)
        {
            trans.localScale = new Vector3(1, -0.3f, 1);
        }

    }
}
