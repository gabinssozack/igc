﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour
{

    public static Dictionary<string, Pool> dictionaryPool = new Dictionary<string, Pool>();

    public static void AddNewPool(Pool pool)
    {
        if(!dictionaryPool.ContainsKey(pool.namePool))
        {
            for (int i = 0; i < pool.total; i++)
            {
                Transform trans = Instantiate(pool.prefab, Vector3.zero, Quaternion.identity);
                pool.elements.Add(trans);
                trans.gameObject.SetActive(false);

            }
            dictionaryPool[pool.namePool] = pool;
        }
        else
        {
            Debug.LogError("already have this kind of bullet");
        }
    }

}
