﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Pool
{
    public int total;
    public string namePool;
    public Transform prefab;

    [System.NonSerialized]
    public List<Transform> elements = new List<Transform>();
    private int index = -1;

    public Pool()
    {

    }
    public Pool(string name, int total, Transform prefab)
    {
        namePool = name;
        this.total = total;
        this.prefab = prefab;
    }

    public Transform OnSpawned()
    {
        index++;
        if (index >= elements.Count)
        {
            index = 0;
        }
        Transform trans = elements[index];
        trans.gameObject.SetActive(true);

        return trans;
    }
    public void OnDespawned(Transform trans)
    {
        if(elements.Contains(trans))
        {
            trans.gameObject.SetActive(false);
        }
    }
}
