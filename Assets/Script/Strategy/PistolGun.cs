﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistolGun : GunBehavior
{
    public override void Setup()
    {

        gunHandle = new IPistolHandle();


        Pool pool = new Pool(projecties.name, clipSize, projecties);
        PoolManager.AddNewPool(pool);

        base.Setup();

    }
}
public class IPistolHandle : IGun
{
    private PistolGun pistol;

    public void FireHandle(object data)
    {
        pistol = (PistolGun)data;

        Debug.Log("Pistol shoot!");

        Transform bullet = pistol.CreateBullet().transform;

        bullet.SetParent(null);
        bullet.position = pistol.muzzle.transform.position;
        bullet.right = pistol.muzzle.transform.right;
        bullet.GetComponent<Bullet>().Setup(pistol);
        pistol.remainBullet--;
        if(pistol.remainBullet <=0)
        {
            pistol.isReload = true;
        }
    }

}