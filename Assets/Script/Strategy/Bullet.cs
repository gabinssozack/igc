﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public int damage;
    public float speed;
    public GunBehavior gun;

    public virtual void Setup(GunBehavior gun)
    {
        this.gun = gun;
        this.damage = gun.damage;
    }

    public virtual void Setup(int damage)
    {
        this.damage = damage;
    }
}
