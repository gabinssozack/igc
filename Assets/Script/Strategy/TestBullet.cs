﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBullet : Bullet
{
    public LayerMask mask;

    public override void Setup(GunBehavior gun)
    {
        base.Setup(gun);
        gameObject.GetComponent<Rigidbody2D>().velocity = transform.right * speed;
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.right, 0.35f,mask);
        if(hit.collider!=null)
        {
            Debug.Log("Bum Bum");
            PoolManager.dictionaryPool[gun.projecties.name].OnDespawned(transform);
            if (LayerMask.LayerToName(hit.collider.gameObject.layer) == "Enemy")
            {
                EnemyControl eC = hit.collider.GetComponent<EnemyControl>();
                eC.OnDamage(this.damage);
                hit.collider.GetComponent<Rigidbody2D>().AddForce(this.transform.right * eC.fallBack);
            }
        }
    }
}
