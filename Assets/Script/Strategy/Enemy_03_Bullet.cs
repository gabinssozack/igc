﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_03_Bullet : Bullet
{
    public LayerMask mask;

    public override void Setup(GunBehavior gun)
    {
        base.Setup(gun);
        gameObject.GetComponent<Rigidbody2D>().velocity = transform.right * speed;
    }

    public override void Setup(int damage)
    {
        base.Setup(damage);
        gameObject.GetComponent<Rigidbody2D>().velocity = transform.right * speed;
    }
    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.right, 0.35f, mask);
        if (hit.collider != null)
        {
            if (hit.collider.CompareTag("Player"))
            {
                PlayerControl player = hit.collider.GetComponent<PlayerControl>();
                if (player.isAvaible)
                {
                    player.OnDamaged(this.damage);
                }
            }
            Destroy(gameObject);

        }
    }
}
