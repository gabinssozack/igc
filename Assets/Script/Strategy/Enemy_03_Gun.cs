﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_03_Gun : GunBehavior
{
    public override void Setup()
    {
        base.Setup();

        gunHandle = new IEnemy_03_Gun();

        Pool pool = new Pool(projecties.name, clipSize, projecties);
        PoolManager.AddNewPool(pool);

    }
}
public class IEnemy_03_Gun : IGun
{
    private Enemy_03_Gun gun;

    public void FireHandle(object data)
    {
        gun = (Enemy_03_Gun)data;

        Debug.Log("Pistol shoot!");

        Transform bullet = gun.CreateBullet().transform;

        bullet.SetParent(null);
        bullet.position = gun.muzzle.transform.position;
        bullet.right = gun.muzzle.transform.right;
        bullet.GetComponent<Bullet>().Setup(gun);
    }
}
