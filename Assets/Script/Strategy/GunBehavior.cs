﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunBehavior : MonoBehaviour
{
    public Transform muzzle;
    public Transform projecties;
    //public Transform impact;
    public int numberBulletPershot = 1;
    public int remainBullet;
    public int recoil;
    public int clipSize;
    public int damage;
    public float reloadTime;
    private float reloadTimeCounter = 0;
    public bool isReload;
    protected IGun gunHandle;

    
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.R) && remainBullet < clipSize)
        {
            isReload = true;
        }
        if(Input.GetMouseButtonDown(0))
        {
            if(!isReload)
            {

                Fire();
            }   
        }
        if(isReload)
        {
            reloadTimeCounter -= Time.deltaTime;
            if(reloadTimeCounter <= 0)
            {
                reloadTimeCounter = reloadTime;
                remainBullet = clipSize;
                UIManager.Instance.UpdateBulletText(clipSize);
                UIManager.Instance.UpdateBulletSlide(0, clipSize);
                UIManager.Instance.UpdateBulletSlide(clipSize);
                isReload = false;
            }
        }
    }

    public virtual void  Setup()
    {
        remainBullet = clipSize;
        reloadTimeCounter = reloadTime;
        UIManager.Instance.UpdateBulletText(clipSize);
        UIManager.Instance.UpdateBulletSlide(0, clipSize);
        UIManager.Instance.UpdateBulletSlide(clipSize);
    }

    public void Fire()
    {
        gunHandle.FireHandle(this);
        PlayerControl.Instance.GetComponent<Rigidbody2D>().AddForce(-GunController.Instance.trans.right * recoil);
        UIManager.Instance.UpdateBulletText(this.remainBullet);
        UIManager.Instance.UpdateBulletSlide(this.remainBullet);
    }
    public Bullet CreateBullet()
    {
        Transform bullet = PoolManager.dictionaryPool[projecties.name].OnSpawned();
       
        return bullet.GetComponent<Bullet>();
    }
}
