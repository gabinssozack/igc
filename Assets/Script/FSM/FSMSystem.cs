﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSMSystem : MonoBehaviour
{
    public FSMState currentState;
    public List<FSMState> states = new List<FSMState>();
    public void AddState(FSMState state)
    {
        Debug.Log("Add State");
        states.Add(state);

        if(states.Count == 1)
        {
            Debug.Log("First State");
            currentState = state;
            state.OnEnter();
        }
        
    }

    public void GoToState(FSMState state)
    {
        if (currentState != null)
        {
            currentState.OnExit();
        }
        currentState = state;
        currentState.OnEnter();
    }

    public void GoToState(FSMState state, object data)
    {
        if (currentState != null)
        {
            currentState.OnExit();
        }
        currentState = state;
        currentState.OnEnter(data);
    }

    private void Update()
    {
        SystemUpdate();
        if (currentState != null)
        {
            currentState.Update();
        }
    }

    private void FixedUpdate()
    {
        SystemFixedUpdate();
        if (currentState != null)
        {
            currentState.FixedUpdate();
        }
    }

    private void LateUpdate()
    {
        SystemLateUpdate();
        if (currentState != null)
        {
            currentState.LateUpdate();
        }
    }
    public virtual void SystemUpdate()
    {

    }

    public virtual void SystemFixedUpdate()
    {

    }

    public virtual void SystemLateUpdate()
    {

    }

}
