﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
public class SpawnerManager : MonoBehaviour
{
    private static SpawnerManager _instance;
    public static SpawnerManager Instance => _instance;
    [SerializeField]
    private Transform[] gunSpawnPos;
    [SerializeField]
    private Transform[] enemySpawnPos;
    [SerializeField]
    private int minTimeGunSpawn;
    public int MinTimeGunSpawn => minTimeGunSpawn;
    [SerializeField]
    private int maxTimeGunSpawn;
    public int MaxTimeGunSpawn => maxTimeGunSpawn;
    [SerializeField]
    private int minEnemyGunSpawn;
    public int MinEnemyGunSpawn => minEnemyGunSpawn;
    [SerializeField]
    private int maxEnemyGunSpawn;
    public int MaxEnemyGunSpawn => maxEnemyGunSpawn;

    private static string GUN_PATH = "Guns/GunCollectable/";
    private static string ENEMY_PATH= "Enemies/";

    private GameObject[] gunObj;
    private GameObject[] enemyObj;

    private List<int> gunPosSpawned, gunPosSpawnedAvailable, enemyPosSpawned, enemyPosSpawnedAvailable;

    private void Awake()
    {
        if (_instance == null) _instance = this;
    }
    private void Start()
    {
        gunPosSpawned = new List<int>();
        gunPosSpawnedAvailable = new List<int>();
        enemyPosSpawned = new List<int>();
        enemyPosSpawnedAvailable = new List<int>();
        for (int i = 0; i < enemySpawnPos.Length; i++)
        {
            enemyPosSpawnedAvailable.Add(i);
        }
        for (int i = 0; i < gunSpawnPos.Length; i++)
        {
            gunPosSpawnedAvailable.Add(i);
        }

        gunObj = Resources.LoadAll(GUN_PATH, typeof(GameObject)).Cast<GameObject>()
     .ToArray();
        enemyObj = Resources.LoadAll(ENEMY_PATH, typeof(GameObject)).Cast<GameObject>()
     .ToArray();


        var gunObjSpawed = GameObject.Instantiate(gunObj[UnityEngine.Random.Range(0, gunObj.Length)]);


        Vector3 gunPos = NewGunPos;
        gunObjSpawed.transform.position = gunPos;

        StartCoroutine(CreateEnemy());
    }
    public Vector3 NewGunPos
    {
        get
        {
            if (gunSpawnPos.Length == 0) return Vector3.zero;
            int randomPos = gunPosSpawnedAvailable[UnityEngine.Random.Range(0, gunPosSpawnedAvailable.Count)];

            return gunSpawnPos[randomPos].position;

        }
    }
    private Vector3 NewEnemyPosition(out int indexPos)
    {
        indexPos = -1;
        if (enemySpawnPos.Length == 0) return Vector3.zero;
        if (enemyPosSpawnedAvailable.Count == 0) return Vector3.zero;
        int randomPos = UnityEngine.Random.Range(0, enemyPosSpawnedAvailable.Count);


        var result = enemySpawnPos[enemyPosSpawnedAvailable[randomPos]].position;
        int vitriLay = enemyPosSpawnedAvailable[randomPos];
        enemyPosSpawnedAvailable.RemoveAt(randomPos);
        enemyPosSpawned.Add(vitriLay);
        indexPos = vitriLay;
        return result;
    }
    public Vector3 NewEnemyPos
    {
        get
        {
            if (enemySpawnPos.Length == 0) return Vector3.zero;
            if (enemyPosSpawnedAvailable.Count == 0) return Vector3.zero;
            int randomPos = UnityEngine.Random.Range(0, enemyPosSpawnedAvailable.Count);

            int vitriLay = enemyPosSpawnedAvailable[randomPos];
            enemyPosSpawnedAvailable.RemoveAt(randomPos);
            enemyPosSpawned.Add(vitriLay);

            return enemySpawnPos[enemyPosSpawnedAvailable[randomPos]].position;

        }
    }



    int timeCreateGun, timeCreateEnemy;
    public IEnumerator CreateGun(Action<GameObject> action)
    {
        timeCreateGun = UnityEngine.Random.Range(minTimeGunSpawn, maxTimeGunSpawn);

        yield return new WaitForSeconds(timeCreateGun);


        Vector3 gunPos = NewGunPos;


        var gunObjSpawed = GameObject.Instantiate(gunObj[UnityEngine.Random.Range(0, gunObj.Length)]);

        gunObjSpawed.transform.position = gunPos;

        action?.Invoke(gunObjSpawed);
        yield return null;
    }

    public IEnumerator CreateEnemy()
    {
        timeCreateEnemy = UnityEngine.Random.Range(minEnemyGunSpawn, maxEnemyGunSpawn);

        yield return new WaitForSeconds(timeCreateEnemy);
        //create 2 enemy

        int index;
        Vector3 enemyPos = NewEnemyPosition(out index);


        var enemyObjSpawed = GameObject.Instantiate(enemyObj[UnityEngine.Random.Range(0, enemyObj.Length)]);
        enemyObjSpawed.GetComponent<EnemyControl>().Setup(null, () =>{
            enemyPosSpawnedAvailable.Add(index);
        });
        enemyObjSpawed.transform.position = enemyPos;


        enemyPos = NewEnemyPosition(out index);


        enemyObjSpawed = GameObject.Instantiate(enemyObj[UnityEngine.Random.Range(0, enemyObj.Length)]);
        enemyObjSpawed.GetComponent<EnemyControl>().Setup(null, () => {
            enemyPosSpawnedAvailable.Add(index);
        });
        enemyObjSpawed.transform.position = enemyPos;



        StartCoroutine(CreateEnemy());

        //gunPos = NewGunPos;


        //gunObjSpawed = GameObject.Instantiate(gunObj[UnityEngine.Random.Range(0, gunObj.Length)]);

        //gunObjSpawed.transform.position = gunPos;


        yield return null;
    }

}
