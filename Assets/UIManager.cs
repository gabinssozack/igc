﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    private static UIManager instance;
    public static UIManager Instance => instance;
    public TextMeshProUGUI score;
    public List<Image> listHeart;
    public TextMeshProUGUI amoutOfBullet;
    public Slider bulletSlide;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        
    }
    private void Start()
    {
        Init();
    }
    public void deactiveHeart(int index)
    {
        listHeart[index].gameObject.SetActive(false);
    }
    public void Init()
    {
        score.text = "0";
        foreach(Image e in listHeart)
        {
            e.gameObject.SetActive(true);
        }
    }

    public void UpdateBulletText(int amout)
    {
        amoutOfBullet.text = amout.ToString();
    }
    public void UpdateScoreText(int amout)
    {
        score.text = amout.ToString();
    }
    public void UpdateBulletSlide(int amount)
    {
        bulletSlide.value = amount;
    }
    public void UpdateBulletSlide(int min, int max)
    {
        bulletSlide.minValue = min;
        bulletSlide.maxValue = max;
    }
}
